export class Result {
	constructor() {
		this.isSuccess = false
		this.message = ""
	}

	isSuccess: boolean
	message: string
}

export class ApiResult<T> extends Result {
	constructor() {
		super()
		this.isSuccess = false
		this.message = ""
		this.data = null
	}

	data: T | null
	isSuccess: boolean
	message: string
	totalPages?: number
	totalCount?: number
	pageNumber?: number
	pageSize?: number
}
